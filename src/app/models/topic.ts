import {Question} from './question';

export class Topic {
  id: number;
  name: string;
  description: string;
  video: string;
  end: number;
  active: number;
  questions: Question[];
}
