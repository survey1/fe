import {Answer} from './answer';

export class Question {
  id: number;
  name: string;
  topic_id: number;
  answers: Answer[];
}
