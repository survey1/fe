export class Answer {
  id: number;
  name: string;
  count: number;
  question_id: number;
}
