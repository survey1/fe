import { Component, OnInit } from '@angular/core';
import {TopicService} from '../services/topic.service';
import {Topic} from '../models/topic';
import {AuthService} from '../auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-mc',
  templateUrl: './mc.component.html',
  styleUrls: ['./mc.component.css']
})
export class McComponent implements OnInit {

  topic: Topic;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  success = '';

  constructor(
    private topicService: TopicService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.init();
  }

  async init() {
    const res: any = await this.topicService.getActiveTopic();
    console.log(res);
    this.topic = res.data;
  }

  async endTopic() {
    this.submitted = true;
    this.loading = true;
    const res: any = await this.topicService.endTopic(this.topic.id);
    console.log('res end topic');
    console.log(res);
    if (res && res.success) {
      this.success = 'Kết thúc chủ đề thành công!';
    } else {
      this.error = 'Kết thúc chủ đề không thành công';
    }
    this.loading = false;
    this.submitted = false;
    this.init();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
