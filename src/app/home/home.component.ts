import { Component, OnInit } from '@angular/core';
import {TopicService} from '../services/topic.service';
import {Topic} from '../models/topic';
import {Answer} from '../models/answer';
import {AnswerService} from '../services/answer.service';
import {Question} from '../models/question';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  player: YT.Player;
  activeTopic: Topic;
  video: string;
  videoState: number;
  questions: Question[];
  questionsClone: any[];
  finish = false;
  constructor(
    private topicService: TopicService,
    private answerService: AnswerService
  ) { }

  ngOnInit() {
    // clear storage
    localStorage.removeItem('answers');
    this.getActiveTopic();
  }
  async getActiveTopic() {
    const topic: any = await this.topicService.getActiveTopic();
    console.log(topic);
    if (topic.data) {
      this.activeTopic = topic.data;
      // console.log(this.activeTopic);
      this.video = topic.data.video;
      this.savePlayer(this.player);
      this.questions = topic.data.questions;
      this.questionsClone = topic.data.questions;
      // add property isHide to first of question cloned
      for (const questionClone of this.questionsClone) {
        questionClone.isHide = true;
      }
      this.questionsClone[0].isHide = false;
    }
  }
  savePlayer(player) {
    this.player = player;
  }
  onStateChange(event) {
    this.videoState = event.data;
    // console.log('player state', event.data);
  }
  userAnswer(answer: Answer, questionCloneId: number) {
    // console.log('answer here');
    // console.log(answer);
    // console.log('question id: ' + questionCloneId);
    // get answers from local storage
    // if exist, check value
      // if value exist, get value (previous value) then send to server together with current answer, update answer value
      // else just send current answer value to server
    // else new item then save to local storage
    const previousAnswer = localStorage.getItem('answers');
    if (previousAnswer) {
      // console.log('answered');
      const prAnswers = JSON.parse(previousAnswer);
      // check if user answer this question before
      const existedAnswer = prAnswers.find(pA => pA.question_id === answer.question_id);
      // console.log(existedAnswer);
      // if existed
      if (existedAnswer) {
        // console.log('ton tai');
        if (existedAnswer.answer_id !== answer.id) {
          // console.log('khac cau tl cu');
          this.updateCountAnswer(answer.id, 1);
          this.updateCountAnswer(existedAnswer.answer_id, -1);
          // existedAnswer.answer_id = answer.id;
          // rebuild prAnswers
          const newPrAnswers = prAnswers.filter(obj => obj.answer_id !== existedAnswer.answer_id);
          newPrAnswers.push({question_id: answer.question_id, answer_id: answer.id});
          localStorage.setItem('answers', JSON.stringify(newPrAnswers));
        }
      } else {      // else, add to list previous answers
        prAnswers.push({question_id: answer.question_id, answer_id: answer.id});
        localStorage.setItem('answers', JSON.stringify(prAnswers));
        this.updateCountAnswer(answer.id, 1);
      }
    } else {
      // console.log('not answered');
      const prAnswers = [
          {
            question_id: answer.question_id,
            answer_id: answer.id
          }
      ];
      localStorage.setItem('answers', JSON.stringify(prAnswers));
      this.updateCountAnswer(answer.id, 1);
    }

    // update isAnswered to question cloned
    const questionClonedIndex = this.questionsClone.findIndex(q => q.id === questionCloneId);
    // console.log('question clone found');
    // console.log(questionClonedIndex);
    if (questionClonedIndex !== undefined) {
      this.questionsClone[questionClonedIndex].isHide = true;
      const nextIndex = questionClonedIndex + 1;
      if (nextIndex < (this.questionsClone.length)) {
        this.questionsClone[nextIndex].isHide = false;
      } else {
        this.finish = true;
      }
    }
  }
  async updateCountAnswer(answerId, count) {
    const a  = await this.answerService.updateCountAnswer(answerId, count);
    // console.log(a);
  }
}
