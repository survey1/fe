import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {map, tap} from 'rxjs/operators';
@Injectable({ providedIn: 'root' })
export class AuthService {
  apiUrl: string = environment.apiUrl;
  constructor(
      public jwtHelper: JwtHelperService,
      private httpClient: HttpClient
    ) {}

  public clear() {
    localStorage.clear();
  }
  //
  public isAuthenticated(): boolean {
    const token = localStorage.getItem('access_token');

    // Check whether the token is expired and return
    // true or false
    return !this.jwtHelper.isTokenExpired(token);
  }

  public login(name: string, password: string) {
    return this.httpClient.post<any>(`${this.apiUrl}/login`, {name, password}).pipe(
      tap(res => {
        localStorage.setItem('access_token', res.access_token);
        localStorage.setItem('role', btoa(res.role));
        return res;
      })
    );
  }

  public logout() {
    this.clear();
  }
}
