import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Topic} from '../models/topic';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class TopicService {
  apiUrl: string = environment.apiUrl;
  token: string;
  headers: HttpHeaders;
  constructor(private httpClient: HttpClient) {
    this.token = localStorage.getItem('access_token');
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + this.token,
    });
  }
  public getActiveTopic() {
    return this.httpClient.get(`${this.apiUrl}/active-topic`).toPromise();
  }

  public getListTopic() {
    return this.httpClient.get(`${this.apiUrl}/topic`, {headers: this.headers}).toPromise();
  }

  public createTopic(data) {
    return this.httpClient.post(`${this.apiUrl}/topic`, data, {headers: this.headers}).toPromise();
  }

  public getDetailTopic(id) {
    return this.httpClient.get(`${this.apiUrl}/topic/${id}`).toPromise();
  }

  public updateTopic(id, data) {
    return this.httpClient.put(`${this.apiUrl}/topic/${id}`, data, {headers: this.headers}).toPromise();
  }

  public activeTopic(id) {
    return this.httpClient.put(`${this.apiUrl}/active-topic/${id}`, {}, {headers: this.headers}).toPromise();
  }

  public endTopic(id) {
    return this.httpClient.put(`${this.apiUrl}/end-topic/${id}`, {}, {headers: this.headers}).toPromise();
  }
}
