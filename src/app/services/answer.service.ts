import {environment} from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class AnswerService {
  apiUrl: string = environment.apiUrl;
  constructor(private httpClient: HttpClient) {}
  public updateCountAnswer(answerId, count: number) {
    console.log(13);
    return this.httpClient.put(`${this.apiUrl}/answer/${answerId}`, {number: count}).toPromise();
  }
}
