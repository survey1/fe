import { Routes, CanActivate } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { HomeComponent} from './home/home.component';
import {McComponent} from './mc/mc.component';

import {
  AuthGuardService as AuthGuard
} from './auth/auth-guard.service';
import {
  RoleGuardService as RoleGuard
} from './auth/role-guard.service';
import {LoginComponent} from './login/login.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
export const ROUTES: Routes = [
  { path: 'login', component: LoginComponent},
  // { path: '', redirectTo: '', pathMatch: 'full' },
  { path: '', component: HomeComponent },
  { path: 'admin', redirectTo: 'admin', pathMatch: 'full'},
  // {
  //   path: 'admin',
  //   component: AdminComponent,
  //   canActivate: [RoleGuard],
  //   data: {
  //     expectedRole: 'admin'
  //   }
  // },
  {
    path: 'mc',
    component: McComponent,
    canActivate: [RoleGuard],
    data: {
      expectedRole: 'mc'
    }
  },
  { path: '**', component: PageNotFoundComponent }
];
