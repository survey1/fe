import { Component, OnInit } from '@angular/core';
import {TopicService} from '../../services/topic.service';
import {Topic} from '../../models/topic';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.css']
})
export class TopicComponent implements OnInit {
  topics: Topic[];
  constructor(
    private topicService: TopicService
  ) { }

  ngOnInit() {
    this.getListTopic();
  }

  async getListTopic() {
    const res: any = await this.topicService.getListTopic();
    if (res && res.success) {
      this.topics = res.data;
    }
  }

  deleteTopic(id) {

  }
}
