import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TopicService} from '../../services/topic.service';

@Component({
  selector: 'app-create-topic',
  templateUrl: './create-topic.component.html',
  styleUrls: ['./create-topic.component.css']
})
export class CreateTopicComponent implements OnInit {
  topicForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  success = '';
  constructor(
    private formBuilder: FormBuilder,
    private topicService: TopicService
  ) { }

  ngOnInit() {
    this.topicForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: [''],
      video: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.topicForm.controls; }

  async onSubmit() {
    this.submitted = true;
    if (this.topicForm.invalid) {
      return;
    }

    this.loading = true;
    const data = {
      name: this.f.name.value,
      description: this.f.description.value,
      video: this.f.video.value
    };
    const res: any = await this.topicService.createTopic(data);
    console.log('res create topic');
    console.log(res);
    if (res && res.success) {
      this.success = 'Thêm mới topic thành công!';
    } else {
      this.error = 'Thêm mới không thành công';
    }
    this.loading = false;
    this.submitted = false;
    this.topicForm.reset();
  }

}
