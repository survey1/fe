import {Routes} from '@angular/router';
import {AdminComponent} from './admin.component';
import {RoleGuardService} from '../auth/role-guard.service';
import {TopicComponent} from './topic/topic.component';
import {CreateTopicComponent} from './create-topic/create-topic.component';
import {EditTopicComponent} from './edit-topic/edit-topic.component';
import {StatisticTopicComponent} from './statistic-topic/statistic-topic.component';

export const adminRoutes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [RoleGuardService],
    data: {
      expectedRole: 'admin'
    },
    children: [
      {path: '', redirectTo: 'topic', pathMatch: 'full'},
      { path: 'topic', component: TopicComponent},
      { path: 'topic/create', component: CreateTopicComponent},
      { path: 'topic/edit/:id', component: EditTopicComponent},
      { path: 'topic/statistic/:id', component: StatisticTopicComponent}
    ]
  }
];
