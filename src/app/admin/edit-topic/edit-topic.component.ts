import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TopicService} from '../../services/topic.service';
import {Topic} from '../../models/topic';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Question} from '../../models/question';

@Component({
  selector: 'app-edit-topic',
  templateUrl: './edit-topic.component.html',
  styleUrls: ['./edit-topic.component.css']
})
export class EditTopicComponent implements OnInit {
  id: number;
  topic: Topic;

  topicForm: FormGroup;
  questions: FormArray;
  answers: FormArray;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';
  success = '';
  constructor(
    private route: ActivatedRoute,
    private topicService: TopicService,
    private formBuilder: FormBuilder
  ) { }

   ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.init();
  }

  async init() {
    const res: any = await this.topicService.getDetailTopic(this.id);
    // console.log(res);
    this.topic = res.data;
    this.topicForm = this.formBuilder.group({
      name: [this.topic.name, Validators.required],
      description: [this.topic.description],
      video: [this.topic.video, Validators.required],
      questions: this.formBuilder.array([])
    });
    this.addQuestion(res.data.questions);
    // console.log(this.questions);
  }

  // convenience getter for easy access to form fields
  get f() { return this.topicForm.controls; }

  // for formArray
  get questionsFormArray() { return this.topicForm.get('questions') as FormArray; }

  addQuestion(questions): void {
    this.questions = this.topicForm.get('questions') as FormArray;
    if (questions && questions.length) {
      for (const question of questions) {
        this.questions.push(this.createQuestion(question));
      }
    } else {
      this.questions.push(this.createQuestion(null));
    }
  }

  createQuestion(question: Question): FormGroup {
    if (question) {
      return this.formBuilder.group({
        name: [question.name, Validators.required],
        answers: this.createAnswer(question.answers)
      });
    } else {
      return this.formBuilder.group({
        name: ['', Validators.required],
        answers: this.createAnswer(null)
      });
    }
  }

  createAnswer(answers = null): FormArray {
    const a = this.formBuilder.array([]);
    if (answers && answers.length) {
      for (const answer of answers) {
        const aFormGroup = this.formBuilder.group({
          name: [answer.name, Validators.required],
          type: [answer.type]
        });
        a.push(aFormGroup);
      }
    } else {
      for (let i = 0; i < 4; i++) {
        let type = 'A';
        if (i === 1) {
          type = 'B';
        } else if (i === 2) {
          type = 'C';
        } else if (i === 3) {
          type = 'D';
        }
        const aFormGroup = this.formBuilder.group({
          name: ['', Validators.required],
          type: [type]
        });
        a.push(aFormGroup);
      }
    }
    return a;
  }

  async onSubmit() {
    this.submitted = true;
    if (this.topicForm.invalid) {
      return;
    }
    // console.log(this.topicForm.value);
    // return;

    this.loading = true;
    const data = this.topicForm.value;
    const res: any = await this.topicService.updateTopic(this.id, data);
    // console.log('res update topic');
    // console.log(res);
    if (res && res.success) {
      this.success = 'Sửa topic thành công!';
    } else {
      this.error = 'Sửa topic không thành công';
    }
    this.loading = false;
    this.submitted = false;
  }

  async activeTopic() {
    const res: any = await this.topicService.activeTopic(this.id);
    // console.log('res active topic');
    // console.log(res);
    if (res && res.success) {
      alert('Active topic thành công!');
    } else {
      alert('Active topic không thành công');
    }
  }
}
