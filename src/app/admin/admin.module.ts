import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { QuestionComponent } from './question/question.component';
import { TopicComponent } from './topic/topic.component';
import {RouterModule} from '@angular/router';
import {adminRoutes} from './admin.routes';
import {RoleGuardService} from '../auth/role-guard.service';
import { AdminComponent } from './admin.component';
import { CreateTopicComponent } from './create-topic/create-topic.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { EditTopicComponent } from './edit-topic/edit-topic.component';
import { StatisticTopicComponent } from './statistic-topic/statistic-topic.component';

import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    AdminComponent,
    NavbarComponent,
    QuestionComponent,
    TopicComponent,
    CreateTopicComponent,
    EditTopicComponent,
    StatisticTopicComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(adminRoutes),
    ChartsModule
  ],
  providers: [
    RoleGuardService
  ]
})
export class AdminModule { }
