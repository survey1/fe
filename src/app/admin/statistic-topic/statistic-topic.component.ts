import { Component, OnInit } from '@angular/core';
import {TopicService} from '../../services/topic.service';
import {Topic} from '../../models/topic';
import {ActivatedRoute} from '@angular/router';
import {Question} from '../../models/question';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { Label } from 'ng2-charts';

@Component({
  selector: 'app-statistic-topic',
  templateUrl: './statistic-topic.component.html',
  styleUrls: ['./statistic-topic.component.css']
})
export class StatisticTopicComponent implements OnInit {

  id: number;
  topic: Topic;
  questions: Question[];
  clonedQuestions: any[];

  // chart
  barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  barChartLabels: Label[] = ['A', 'B', 'C', 'D'];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [pluginDataLabels];

  barChartData: ChartDataSets[] = [
    { data: [45, 37, 60, 70], label: 'Số người chọn' }
  ];

  constructor(
    private route: ActivatedRoute,
    private topicService: TopicService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.init();

    // init chart
  }

  async init() {
    const res: any = await this.topicService.getDetailTopic(this.id);
    this.topic = res.data;
    this.questions = res.data.questions;
    this.clonedQuestions = res.data.questions;
    for (const question of this.clonedQuestions) {
      const dataOfChart = [];
      for (const answer of question.answers) {
        dataOfChart.push(answer.count);
      }
      question.data_chart = [
        {data: dataOfChart, label: 'Số người chọn'}
      ];
    }
  }

}
