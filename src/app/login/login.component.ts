import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../auth/auth.service';
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
  ) {
    // redirect to mc or admin route if logged in
    if (this.authService.isAuthenticated()) {
      const token = localStorage.getItem('access_token');
      const role = atob(localStorage.getItem('role'));

      if (token) {
        if (role === 'admin') {
          this.router.navigate(['/admin']);
        } else if (role === 'mc') {
          this.router.navigate(['/mc']);
        }
      }
    } else {
      localStorage.removeItem('access_token');
      localStorage.removeItem('role');
    }
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      name: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['return-Url'] || '/';
    console.log(this.returnUrl);
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authService.login(this.f.name.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          console.log('res');
          console.log(data);
          const role = data.role;
          if (role === 'admin') {
            this.router.navigate(['/admin']);
          } else if (role === 'mc') {
            this.router.navigate(['/mc']);
          }
        },
        err => {
          this.error = err;
          this.loading = false;
        }
      );

  }
}
